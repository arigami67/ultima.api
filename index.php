<?php
require '../../autoload.php';
require 'const.php';
require 'config.php'; // Настройка idiorm

function auth()
{
    $valid_passwords = array("contman" => "contman");
    $valid_users = array_keys($valid_passwords);

    $user = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : '';
    $pass = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : '';

    $validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

    if (!$validated) {
        header('WWW-Authenticate: Basic realm="wtF?"');
        header('HTTP/1.0 401 Unauthorized');
        die ("(");
    }
}
auth();

use ORM as ORM;
use Radm\Helper\RFile as RFile;
use Radm\Helper\Request as Request;
$rFile = new RFile();
$app   = new \Slim\Slim(
    array( 'view' => new \Slim\Views\Twig())
);


$view  = $app->view();
$view->parserOptions = array();
$view->parserExtensions = array( new \Slim\Views\TwigExtension());
$app->get('/', function ($name = null) use ($app, $rFile){
    $app->redirect('/pages', 301);
});
$app->get('/pages/:name', function ($name = null) use ($app, $rFile){
    require $rFile->check($name);
    $app->render($name, array(
        'array' => isset($array) ? $array : array(),
        'base'  => isset($base)  ? $base  : array(),
        'menus' => [
            [
                'list' => 0, 'href' => 'index.html', 'fa' => 'fa-dashboard', 'title' => 'Главная'
            ],
            [
                'list' => 1, 'href' => '3', 'fa' => 'fa-bar-chart-o', 'title' => 'Редактирование', 'li' =>
                [
                    ['КАКи c редактором', '/pages/pd.html?name=howto1&load=pd'],
                    ['Свободные картинки', '/pages/pd.html?name=upload_pics&load=pd'],
                    ['Шаблоны', '/pages/pd.html?name=templates&load=pd'],
                    ['Страницы', '/pages/pd.html?name=pages3&load=pd'],
                    ['Разделы', '/pages/pd.html?name=content_categories&load=pd'],
                    ['Группы разделов', '/pages/pd.html?name=content_groups&load=pd'],
                    #['Типы клиентов', '/pages/pd.html?name=client_categories'],
                ]
            ],
            [
                'list' => 1, 'href' => '3', 'fa' => 'fa-bar-chart-o', 'title' => 'Мониторинг', 'li' =>
                [
                    ['Поисковая популярность', '/pages/pd.html?name=search_popularity&load=pd'],
                    ['Журнал ошибок', '/pages/pd.html?name=logger&load=pd'],
                ]
            ],
            [
                'list' => 1, 'href' => '3', 'fa' => 'fa-bar-chart-o', 'title' => 'SEO', 'li' =>
                [
                    ['Мета данные', '/pages/pd.html?name=meta&load=pd'],
                    ['Сео тексты категорий', '/pages/pd.html?name=cats_seo&load=pd'],
                    ['Сео тексты брендов', '/pages/pd.html?name=brands_seo&load=pd'],
                    ['Свободное сео', '/pages/pd.html?name=free_seo&load=pd'],
                    ['Разделение Excel', '/pages/pd.html?name=price_set&load=pd'],
                ]
            ],
            [
                'list' => 1, 'href' => '3', 'fa' => 'fa-bar-chart-o', 'title' => 'АБ-тест', 'li' =>
                [
                    ['Конструктор лендинга', '/pages/pd.html?name=landing&load=pd'],
                    ['Шаблоны товара', '/pages/pd.html?name=template_good'],
                ]
            ],
            [
                'list' => 1, 'href' => '3', 'fa' => 'fa-bar-chart-o', 'title' => 'Мусор', 'li' =>
                [
                    #['Баннеры', '/pages/pd.html?name=banner'],
                    #['Опросы', '/pages/pd.html?name=vote'],
                    ['Константы', '/pages/pd.html?name=constants'],
                    #['Акции(промо)', '/pages/pd.html?name=action'],
                    ['Урлы для меню', '/pages/pd.html?name=vars1&load=pd'],
                    #['Связка URL-ов', '/pages/pd.html?name=page_remap'],
                    #['Кеш сайта', '/pages/pd.html?name=cache'],
                    #['Города', '/pages/pd.html?name=cities'],
                    #['Офисы', '/pages/pd.html?name=offices'],
                    #['КАКи', '/pages/pd.html?name=howto'],
                ]
            ],
            [
                'list' => 1, 'href' => '2', 'fa' => 'fa-bar-chart-o', 'title' => 'ТК', 'li' =>
                [
                    ['Общая информация', '/pages/logs_total.html'],
                    ['Синхронизация', '/pages/sync.html'],
                    ['Ошибки', '/pages/logs_error.html'],
                    ['Поиск места ТК', '/pages/logs_search_tk.html'],
                    ['Не найденные записей', '/pages/logs_error_off.html'],
                ]
            ],
        ]
    ));
});
$app->get('/:name', function ($name = null) use ($app, $rFile){
    $app->redirect('pages/index.html');
});
$app->post('/pages/:name', function ($name = null) use ($app, $rFile){
    require $rFile->check($name);
});

$app->run();