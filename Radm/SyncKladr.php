<?php
namespace Radm;

use ORM as ORM;
use Ultima\Tk\RTk as RTk;
use Ultima\Tk\RSdek as RSdek;
use Ultima\Tk\RMajor as RMajor;
use Ultima\Tk\ROrmDB as ROrmDB;
use Ultima\Exception\RException as RException;
use Radm\DB\DBKladr_tk as DBKladr_tk;
use Ultima\Helper\RParam as RParam;

set_time_limit(300);
/**
 * Синхронизация Кладра
 * Class Sync
 * @package Radm
 */
class SyncKladr{
    private $i;
    public function kladrRus(){
        $sql ="
            SELECT
              concat(a.code1,'-',a.code2,'-',a.code3) as code,
              a.name as city,
              b1.name as region,
              abr.short_name as short
            FROM addresses.kladr_level3 a
              JOIN addresses.kladr_level1 b1 ON b1.code1 = a.code1
              JOIN addresses.kladr_abbrs abr ON b1.kladr_abr_id = abr.id
            ;
        ";
        $array = ORM::for_table('')->raw_query($sql)->find_array();

        return $array;
    }
    /*
     * Синхронизация таблиц с kladr_tk
     */
    public function synchronization($tk, $country){

        switch($country['name']){
            case 'rus':
                $kladr = $this->kladrRus();
                break;
            default:
                $kladr = array();
        }

        foreach($kladr as $k=>$row){
            $code = $row['code'];

            try {
                switch($tk){
                    case RParam::$TK_SDEK:
                        $class = new RSdek;
                        break;
                    case RParam::$TK_MAJOR:
                        $class = new RMajor;
                        break;
                    default:
                        return false;
                }

                $sdek = new RTK($class, new ROrmDB(ORM::for_table('')));

                $sdek->setCountry($country['name']);
                $sdek->processCity($code);

                $id     = $sdek->city(array());
                if($id){
                    $c = explode('-', $code);
                    if(!DBKladr_tk::getInstance()->init()->where(array(
                        'code1'  => $c[0],
                        'code2'  => $c[1],
                        'code3'  => $c[2],
                        'tk'     => $tk,
                        'country'=> $country['id']
                    ))->count()){
                        DBKladr_tk::getInstance()->insert($c[0], $c[1], $c[2], $tk, $id, DBKladr_tk::$IT_WORK, $country['id']);
                    }
                }
            } catch (RException $e) {

            }
        }
    }

    public function insert(){

    }

}