<?php
namespace Radm;

use ORM as ORM;

/**
 * Общий лог
 * Class Logs
 * @package Radm
 */
class LogsInformation{
    public function countKladr(){
        $sql ="
            SELECT
              concat(a.code1,'-',a.code2,'-',a.code3) as code,
              a.name as city,
              b1.name as region,
              abr.short_name as short
            FROM addresses.kladr_level3 a
              JOIN addresses.kladr_level1 b1 ON b1.code1 = a.code1
              JOIN addresses.kladr_abbrs abr ON b1.kladr_abr_id = abr.id
            ;
        ";
        $count = ORM::for_table('')->raw_query($sql)->find_many()->count();

        return $count;
    }

    /**
     * Количество от каждого ТК
     * @param $tk ТК
     * @return int
     */
    public function countTk($tk){
        $sql ="
            SELECT
              id
            FROM kladr_tk
            where tk=".$tk."
            ;
        ";
        $count = ORM::for_table('')->raw_query($sql)->find_many()->count();

        return $count;
    }
}