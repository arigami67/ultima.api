<?php
namespace Radm\DB;
use Radm\Abstracts\aDB as aDB;

class DBMajor extends aDB{
    protected $name = 'major';
    private static $instance;
    /**
     * // Возвращает единственный экземпляр класса. @return Singleton
     * @return Hb_tk
     */
    public static function getInstance() {
        if ( empty(self::$instance) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}