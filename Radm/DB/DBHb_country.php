<?php
namespace Radm\DB;

use Radm\Abstracts\aDB;

class DBHb_country extends aDB{
    protected $name = 'hb_country';
    private static $instance;
    /**
     * // Возвращает единственный экземпляр класса. @return Singleton
     * @return Hb_tk
     */
    public static function getInstance() {
        if ( empty(self::$instance) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getName(){return $this->name;}

}