<?php
namespace Radm\DB;

use ORM as ORM;
use Radm\Abstracts\aDB;

class DBKladr_tk extends aDB{

    protected $name = 'kladr_tk';
    public static $IT_WORK    = 1;
    public static $IT_NO_WORK = 0;
    /**
     * Кем была заполнена таблица
     * @var int
     */
    public static $ROBOT_ON    = 1;
    public static $ROBOT_OFF = 0;

    private static $instance;
    /**
     * // Возвращает единственный экземпляр класса. @return Singleton
     * @return Hb_tk
     */
    public static function getInstance() {
        if ( empty(self::$instance) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    public function insert($code1, $code2, $code3, $tk, $tk_id, $work, $country){
        $person = ORM::for_table($this->name)->create();

        $person->code1  = $code1;
        $person->code2  = $code2;
        $person->code3  = $code3;
        $person->tk     = $tk;
        $person->tk_id  = $tk_id;
        $person->work   = $work;
        $person->robot  = self::$ROBOT_ON;
        $person->country= $country;

        return $person->save();
    }
}