<?php
namespace Radm\DB;
use Radm\Abstracts\aDB;
use Ultima\Helper\RParam as RParam;

class DBHb_tk extends aDB{
    protected $name = 'hb_tk';
    private static $instance;

    /**
     * // Возвращает единственный экземпляр класса. @return Singleton
     * @return Hb_tk
     */
    public static function getInstance() {
        if ( empty(self::$instance) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    /**
     * Выбираем ТК для формы
     */
    public function defaultTk($array, $data){
        if(isset($data['tk'])) {
            $tk = $data['tk'];

            foreach ($array as $value) {
                if ($value['id'] = $tk) {
                    return $tk;
                }
            }
        }

        return RParam::$TK_SDEK;
    }
    public function getName(){return $this->name;}
}