<?php
namespace Radm\DB;

use ORM as ORM;
use Radm\Abstracts\aDB;

class DBKladr_tk_errors extends aDB{
    protected $name = 'kladr_tk_errors';
    public static $IT_WORK    = 1;
    public static $IT_NO_WORK = 0;
    private static $instance;
    /**
     * // Возвращает единственный экземпляр класса. @return Singleton
     * @return Hb_tk
     */
    public static function getInstance() {
        if ( empty(self::$instance) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getName(){return $this->name;}
}