<?php
namespace Radm\Abstracts;
use ORM as ORM;

abstract class aDB{
    public function init(){
        return ORM::for_table($this->name);
    }
    public function getName(){return $this->name;}
}