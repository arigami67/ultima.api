<?php
namespace Radm\Helper;

use Ultima\Helper\RFilter as RFilter;

class RFile{
    public $homeDir = 'class/';
    public $homeFile = 'index.php';

    public function check($file){

        $pathinfo = pathinfo($file);
        $file = RFilter::hString($pathinfo['filename']);

        $filename = $this->homeDir.$file.'.php';
        if (file_exists($filename)) {
            return $filename;
        } else {
            return $this->homeDir.DS.$this->homeFile;
        }

    }
}