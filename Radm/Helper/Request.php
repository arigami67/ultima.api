<?php
namespace Radm\Helper;

use Ultima\Helper\RFilter as RFilter;

/**
 * В помощь роутингу
 * Class Request
 * @package Radm\Helper
 */
class Request{
    private $url;
    private $folder = 'sb-admin';
    private $homeFolder = 'pages';
    private $homeFile = 'index.php';

    public function __construct($url){
        $this->url = RFilter::hString($url);
    }
    public function rRequire(){
        $url = parse_url($this->url);

        $exp = explode('/', $url['path']);
        if(!$exp[1]) return 'index.php';

        return $this->url;
    }
    public function rCheck($file){
        $url = parse_url($this->url);

        $exp = explode('/', $url['path']);
        $exp = array_diff($exp, array(0, null));

        $filename = $this->homeFolder.DS.$exp[1].'.php';

        if (file_exists($filename)) {
            return $filename;
        } else {
            return $this->homeFolder.DS.$this->homeFile;
        }
    }
    public function setHomeUrl($home){$this->homeFile = $home;}
    public function getFolder(){return $this->folder;}
}