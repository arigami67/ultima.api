<?php
namespace Radm\Helper;

use Radm\DB\DBHb_country as DBHbContry;
use Radm\DB\DBHb_tk as DBHbTk;


class RHandBook{
    private static $instance;
    /**
     * // Возвращает единственный экземпляр класса. @return Singleton
     * @return Hb_tk
     */
    public static function getInstance() {
        if ( empty(self::$instance) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    public function getCountry(){
        foreach(DBHbContry::getInstance()->init()->find_result_set() as $value){
            $id = $value['id'];
            $name = $value['name'];
            $array["$id"] = array(
                'id' => $id,
                'name' => $name,
            );
        }
        return $array;
    }
    public function getTk(){
        foreach(DBHbTk::getInstance()->init()->find_result_set() as $value){
            $id = $value['id'];
            $name = $value['name'];
            $array["$id"] = array(
                'id' => $id,
                'name' => $name,
            );
        }

        return $array;
    }
}