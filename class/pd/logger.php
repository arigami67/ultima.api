
<?php
$base['page'] = 'Логер';
$name = $adm_path.'.html';
$p = function($page){
    $sql = "SELECT * FROM ".SHARE_DB_NAME.".admin_logs AS al ORDER BY date_stamp DESC LIMIT ".$page;
    $res = ExecuteSQL($sql);
    $sql_c = "SELECT count(id) AS c FROM ".SHARE_DB_NAME.".admin_logs";
    $res_c = ExecuteSQL($sql_c);
    $row_c = $res_c->fetchRow();

    $return = [];
    while($row = $res->fetchRow()){
        $cart = json_decode($row['cart'], true);
        $login = json_decode($row['agent'], true)['login'];
        $error = $row['line'];
        $string_c = '<table width="100%">';
        $string_c.= '<tr><td colspan=2 style="background:#FFFEC1">'.$error.'</td></tr>';
        foreach($cart as $k=>$v){
            $string_c.= '<tr>';
            $string_c.= '<td>'.$v['amnt'].'</td><td><div><a target="_blank" href="http://partsdirect.ru/goods/'.$v["id"].'">'.$v['name'].'</a></div></td>';
            $string_c.= '</tr>';
        }
        $string_c.= '</table>';

        $return[] = array(
            'id' => $row['date_stamp'],
            'cart' => $string_c,
            'login' => $login,
        );
    }
    return $data = ['data' =>$return, 'count'=> $row_c['c']];
};
$a = $p(200);
$count = $a['count'];
$data = $a['data'];
$array['data'] = $data;
