<?php
$base['page'] = 'Разделы';
$name = $adm_path.'.html';
$server = $GLOBALS['server'];
$array['set_name'] = 'content_categories';

$p = function() use ($server){
    $rows = $server->staticPage->getAllContentCategories();
    foreach ($rows as $key => $value)
    {
        $rows[$key]['pages'] = $server->staticPage->getContentToCategories($value['id']);
    }
    $sql = "SELECT * FROM content_groups";
    $res = ExecuteSQL($sql);
    $content_groups = [];
    while($row = $res->FetchRow())
    {
        $content_groups[$row['id']] = $row;
    }

    return [
        'rows' => $rows,$content_groups,
        'content' => $content_groups,
    ];
};

$array['data'] = $p();
