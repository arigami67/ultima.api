<?php
$base['page'] = 'Каки с редактором';
$name = $adm_path.'.html';
$server = $GLOBALS['server'];

function get_with_cat_alias($request) {
    global $_cat_url_aliases_;

    $url = $request;

    foreach ($_cat_url_aliases_ as $_cat_url_aliases) {
        foreach ($_cat_url_aliases as $key => $value) {
            if (preg_match('~r=' . $value . '$~si', $url)) {
                $url = '/' . $key . '/';
            } elseif (preg_match('~html\?r=' . $value . '~si', $url))// не конечник!
            {
                foreach ($_cat_url_aliases as $key1 => $value1) {
                    if (preg_match('~html\?r=' . $value . ';' . $value1 . '~si', $url)) {$url = preg_replace('~^/cats2/.*?\.html\?(.*?)$~si', '/' . $key . '/' . $key1 . '/' . (isset($_GET['full_model']) ? $_GET['full_model'] . '/' : '') . '?$1', $url);
                                                $subv = $value1;
                                                $double = true;
                                                break;

                                        }
                                }
                                if (!$double) {
                                        $url = preg_replace('~^/cats2/.*?\.html\?(.*?)$~si', '/' . $key . '/?$1', $url);
                                }
                                if (preg_match('~r=1_(.*?)$~si', $url)) {
                                        $url = preg_replace('~\=(1__(\d+)\;?)(1__(\d+)\;?)?~si', '=', $url);
                                        $url = preg_replace('~\?r\=$~si', '', $url);
                                }

                        } elseif (preg_match('~gifts/\?r=' . $value . '~si', $url)) {

                                $url = '/' . $key . '/' . preg_replace('~/gifts/(\?r=' . $value . ';)~si', '?r=', $url);

                        }
                }
        }
        return $url;
}
$p = function() use ($server){
    $sql = "SELECT nsp.name, nsp.id as spid, nsp.parentid, h.id,h.title,h.title as text,h.catid,h.hidden,h.alias FROM " . SHARE_DB_NAME . ".howto_texts h LEFT JOIN " . SHARE_DB_NAME . ".".$server->const->get("active_new_soft_price")." nsp ON h.catid=nsp.id  ORDER BY h.catid";
        $res = ExecuteSQL($sql);
        $rows= [];
        while ($row=$res->FetchRow())
        {
                if (intval($row[catid] > 0))
                {
                        $row['url'] = get_with_cat_alias('/cats2/list.html?r=1__'.($row['parentid']>0? $row['parentid'].';1__' : '').$row['spid']);
                }
                else
                {
                    $row['url'] = '';
                    $row['name'] = 'Общее';
                }
                    $rows[] = $row;
        }

        return $rows;

};
$data = $p();
$array['data'] = $data;
