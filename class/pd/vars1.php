<?php

$base['page'] = 'Категори первого уровня';
$name = $adm_path.'.html';
$server = $GLOBALS['server'];
$array['set_name'] = 'vars1';

$p = function() use ($server){
    $sql = "SELECT nsp.name, nsp.id, nsp.parentid, cst.*, mv.url mvurl FROM " . SHARE_DB_NAME . ".menu_vars cst
                LEFT JOIN " . SHARE_DB_NAME . ".menu_vars_new mv on cst.soft_id=mv.soft_id
          right join " . SHARE_DB_NAME . ".".$server->const->get("active_new_soft_price")." nsp ON cst.soft_id=nsp.id where nsp.level=1 ORDER BY cst.soft_id";
    $res = ExecuteSQL($sql);
    $rows= array();
    while ($row=$res->FetchRow())
    {

        $rows[] = $row;
    }
    return $rows;
};

$array['data'] = $p();
