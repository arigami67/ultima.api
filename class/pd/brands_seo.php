<?php
$base['page'] = 'Бренды в категориях первого уровня';
$name = $adm_path.'.html';
$server = $GLOBALS['server'];
$array['set_name'] = 'brands_seo';

$p = function() use ($server){
    $sql = "SELECT seo.*, brands.brand AS name FROM " . SHARE_DB_NAME . ".brands_seo_texts seo
          join " . SHARE_DB_NAME . ".".$server->const->get("active_brands")." brands ON brands.brandID=seo.brand_id ORDER BY brands.brandID";
    $res = ExecuteSQL($sql);
    $rows= [];
    while ($row=$res->FetchRow())
    {
        $rows[] = $row;
    }
    return $rows;
};
$data['array'] = $p();
