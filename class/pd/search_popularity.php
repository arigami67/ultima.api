<?php
$base['page'] = 'Поисковая популярность';
$name = $adm_path.'.html';
$text = $_GET['text'];
$date1 = $_GET['date1'];
$date2 = $_GET['date2'];

$p = function($page, $text = '', $date1 ='', $date2 = ''){
    $text = $text ? " AND q LIKE '".$text."%'" : "";
    $date1 = $date1 ? $date1 : date("Y-m-d 00:00:00", (time()-(3600*3*24)));
    $date2 = $date2 ? $date2 : date("Y-m-d 23:59:59", time());

    $sql = "SELECT * FROM  search_queries WHERE time BETWEEN '".$date1."' AND '".$date2."' ".$text."  ORDER BY time DESC LIMIT ".$page;
    $res = ExecuteSQL($sql);
    $data = [];
    while($row = $res->fetchRow()){
        $data[] = [
            'q' => '<a target="_blank" href="http://www.partsdirect.ru/search.php?q='.$row['q'].'">'.$row["q"].'</a>',
            'count' => $row['times_requested'],
            'time' => $row['time'],
        ];
    }

    return $data;
};
$array['get_link'] = $array['get'];
$array['data'] = $p(5000, $text, $date1, $date2);
$array['get'] = [
    'text' => $text,
    'date1' => $date1,
    'date2' => $date2,
];
