<?php

$base['page'] = 'Лог ошибок';

use Radm\DB\DBKladr_tk_errors as DBKte;
use Radm\DB\DBHb_tk as DBHbTk;
use Ultima\Tk\RTk as RTk;


if(isset($_POST['r'])){
    if(isset($_POST['delete'])){ // Удаление из Логи
        $id = $_POST['id'];
        if($list = DBKte::getInstance()->init()->where('id', $id)->find_one()){
            $list->delete();
        }
    }elseif(isset($_POST['update'])){ // Ставим метку
        $id = $_POST['id'];
        $query = ORM::for_table('kladr_tk_errors')->where(array('id' => $id))->find_one();
        $query->work = RTk::$LOGS_NO_WORK;

        $query->save();
        return print 1;
    }
}



$sql = '
SELECT
       k.id as id,
       k.country as countryId, country.name as country ,
       CONCAT(k.code1,"-", k.code2, "-", k.code3) as code,
       tk.name as tk,
       tk.id as tkId,
       a.name as city,
       b1.name as obl,
       abr.name as prefix
 FROM '.DBKte::getInstance()->getName().' as k
  JOIN '.DBHbTk::getInstance()->getName().' as tk ON tk.id=k.tk
  JOIN addresses.kladr_level3 as a ON CONCAT(a.code1,"-",a.code2,"-",a.code3) = CONCAT(k.code1,"-",k.code2,"-",k.code3)
  JOIN addresses.kladr_level1 b1 ON b1.code1 = a.code1
  JOIN hb_country country ON country.id = k.country
  JOIN addresses.kladr_abbrs abr ON b1.kladr_abr_id = abr.id
WHERE k.error= 1005
AND k.work=1;
 ;
';

$array['data'] = DBKte::getInstance()->init()->raw_query($sql)->find_array();


/*
foreach(DBKte::init()->raw_query($sql)->find_result_set() as $record) {
    $data = $record;
    $data['error'] =
    $array[] = $data;

}
*/