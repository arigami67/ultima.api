<?php
use Ultima\Helper\RParam as RParam;
use Radm\LogsInformation as LogsInformation;

$inf = new LogsInformation();

$base['page'] = 'Статистика';

$array['count'] = array(
    'kladr' => $inf->countKladr(),
    'sdek'  => $inf->countTk(RParam::$TK_SDEK),
    'major' => $inf->countTk(RParam::$TK_MAJOR),
    'ems'   => $inf->countTk(RParam::$TK_EMS)
);
