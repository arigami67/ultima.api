<?php
$base['page'] = 'PD';
$n = isset($_GET['name']) ? $_GET['name'] : 'banner';

$links = [
    'banner' => 'banner.php',
    'vote' => 'vote.php',
    'landing' => 'landing.php',
    'search_popularity' => 'search_popularity.php',
    'constants' => 'constants.php',
    'price_set' => 'price_set.php',
    'cities' => 'cities.php',
    'offices' => 'offices.php',
    'cats_seo' => 'cats_seo.php',
    'brands_seo' => 'brands_seo.php',
    'free_seo' => 'free_seo.php',
    'howto' => 'howto.php',
    'howto1' => 'howto1.php',
    'action' => 'action.php',
    'upload_pics' => 'upload_pics.php',
    'vars1' => 'vars1.php',
    'templates' => 'templates.php',
    'meta' => 'meta.php',
    'logger' => 'logger.php',
    'pages3' => 'pages3.php',
    'content_categories' => 'content_categories.php',
    'content_groups' => 'content_groups.php',
    'client_categories' => 'client_categories.php',
    'page_remap' => 'page_remap.php',
    'cache' => 'cache.php',
    'template_good' => 'template_good.php',
];

$array['link'] = $links[$n];

if($_GET['load'] == 'pd' || $_GET['load'] == 'iframe') {
    $adm_path = "pd/".$_GET['name'];
    $array['load'] = $_GET['load'];
    $array['get'] = $_SERVER["QUERY_STRING"];
    define('COMMON_DIR', '/home/domains/common');
    require_once(COMMON_DIR . "/implantates/common_setup.ini.php");
    require_once(COMMON_DIR . "/implantates/common_load.php");
    require_once(COMMON_DIR . "/lib/tools.class.php");
    $GLOBALS['server'] = new Server();

    require_once('class/'.$adm_path.'.php');

}


