<?php

$base['page'] = 'Лог ошибок';

use Radm\DB\DBKladr_tk_errors as DBKte;
use Radm\DB\DBHb_tk as DBHbTk;


if(isset($_POST['r'])){
    if(isset($_POST['delete'])){ // Удаление из Логи
        $id = $_POST['id'];
        if($list = DBKte::getInstance()->init()->where('id', $id)->find_one()){
            $list->delete();
        }
    }
}



$sql = '
SELECT
       k.id as id,
       k.country as countryId, country.name as country ,
       CONCAT(k.code1,"-", k.code2, "-", k.code3) as code,
       tk.name as tk,
       tk.id as tkId,
       a.name as city,
       b1.name as obl,
       abr.name as prefix
 FROM '.DBKte::getInstance()->getName().' as k
  JOIN '.DBHbTk::getInstance()->getName().' as tk ON tk.id=k.tk
  JOIN addresses.kladr_level3 as a ON CONCAT(a.code1,"-",a.code2,"-",a.code3) = CONCAT(k.code1,"-",k.code2,"-",k.code3)
  JOIN addresses.kladr_level1 b1 ON b1.code1 = a.code1
  JOIN hb_country country ON country.id = k.country
  JOIN addresses.kladr_abbrs abr ON b1.kladr_abr_id = abr.id
WHERE k.error= 1005
AND k.work=0;
 ;
';

$array['data'] = DBKte::getInstance()->init()->raw_query($sql)->find_array();