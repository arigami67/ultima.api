<?php
$base['page'] = 'Поиск места ТК';

use Radm\DB\DBKladr_tk_errors as DBKte;

use Radm\DB\DBSdek as DBSdek;
use Radm\DB\DBMajor as DBMajor;
use Radm\Helper\RHandBook as RHandBook;
use Radm\DB\DBHb_tk as DBHbTk;
use Radm\DB\DBKladr_tk_errors as DBKladrErrors;
use ORM as ORM;

$defTk = RHandBook::getInstance()->getTk();
unset($defTk["64"]);

$defCountry = RHandBook::getInstance()->getCountry();

if(isset($_POST['r'])){

    $kladr  = $_POST['kladr'];
    $id     = $_POST['id'];
    $tk     = $_POST['tk'];
    $country  = $defCountry[$_POST['country']];

    $k = explode('-', $kladr);

    $one = ORM::for_table('kladr_tk')
        ->where(array(
            'code1' => $k[0],
            'code2' => $k[1],
            'code3' => $k[2],
            'tk'    => $tk,
            'country' => $country['id'],
        ))
        ->count();

    if($one == 0) {
        $person = ORM::for_table('kladr_tk')->create();
        $person->code1 = $k[0];
        $person->code2 = $k[1];
        $person->code3 = $k[2];
        $person->tk    = $tk;
        $person->tk_id = $id;
        $person->country = $country['id'];

        $person->work = DBKladrErrors::$IT_WORK;

        return print $person->save();
    }else{
        return print 0;
    }

}

$rec = isset($_GET['rec']) ? $_GET['rec'] : '';
$country = isset($_GET['country']) ? $_GET['country'] : 'rus';

if(isset($_GET['code'])){
    $code = $_GET['code'];


    switch($country){
        case 'rus':
            $sql = "
            SELECT
              CONCAT(a.code1,'-', a.code2,'-', a.code3) as code,
              a.name as city,
              b1.name as region,
              abr.short_name as short,
              'rus' as country,
              643 as countryId
            FROM addresses.kladr_level3 a
              JOIN addresses.kladr_level1 b1 ON b1.code1 = a.code1
              JOIN addresses.kladr_abbrs abr ON b1.kladr_abr_id = abr.id
            WHERE
            CONCAT(a.code1,'-', a.code2,'-', a.code3) = '" . $code . "'
            ;
        ";

            break;
        case 'kz':
            $sql = "SELECT
                        CONCAT(2,'-', 2,'-', a.id) as code,
                        name as region,
                        '' as city,
                        '' as short,
                        'kz' as country,
                        398 as countryId
                    FROM addresses.kz_cities a
                    WHERE CONCAT(2,'-', 2,'-', a.id) = '" . $code . "'
                    ;";
            break;
        default:
            $sql = null;
    }


    $kladr = $sql != null
        ? ORM::for_table('')->raw_query($sql)->find_array()
        : array();

}
$defCountry = RHandBook::getInstance()->getCountry();




$paramTk = DBHbTk::getInstance()->defaultTk($defTk, $_GET);
$sdek = DBSdek::getInstance()->init()->find_array();
$array = array(
    \Ultima\Helper\RParam::$TK_SDEK  => DBSdek::getInstance()->getName(),
    \Ultima\Helper\RParam::$TK_MAJOR => DBMajor::getInstance()->getName(),
);
switch($paramTk){
    case \Ultima\Helper\RParam::$TK_SDEK:

        $sqlTk = '
            SELECT *
             FROM '.$array[$paramTk].'
             WHERE
                country = "'.$country.'"
             ;
        ';

        break;
    case \Ultima\Helper\RParam::$TK_MAJOR:
        $sqlTk = '
            SELECT
            id,
            city as city,
            city1 as obl
             FROM '.$array[$paramTk].'
             ;
        ';
        break;
    default:
        $sqlTk = null;
}

if($sqlTk == null){
    $table = array();
}else{
    $table = ORM::for_table('')->raw_query($sqlTk)->find_array();
}


$array['param']['tk']        = $paramTk;
$array['param']['rec']       = $rec;
$array['data']['country']    = $defCountry;
$array['data']['tk']         = $defTk;
$array['data']['table']      = $table;
$array['data']['kladr']      = isset($kladr) ? $kladr : array() ;
