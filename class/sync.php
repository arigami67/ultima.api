<?php

$base['page'] = 'Синхронизация';

use Radm\SyncKladr as Sync;
use Ultima\Helper\RParam as RParam;
use Radm\Helper\RHandBook as RHandBook;
use Radm\DB\DBHb_tk as DBHbTk;

$default['country'] = isset($_GET['country']) ? $_GET['country'] : 643;
$country = RHandBook::getInstance()->getCountry();

$tk = RHandBook::getInstance()->getTk();
unset($tk["64"]);

if(isset($_POST['r'])){
    $post = array(
        'tk'      => $_POST['tk'],
        'country' => $_POST['country'],
    );


    if(!isset($country[$post['country']])) return false;
    $country = $country[$post['country']];
    $tk = $post['tk'];

    $sync = new Sync();
    $sync->synchronization($tk, $country);
    return print 1;
}


$array['data']['country']    = $country;
$array['data']['tk']         = $tk;