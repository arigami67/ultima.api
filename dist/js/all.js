function dataTablesLe(table){
    var obj = {
        id : table.find('.table-id').text(),
        tr : table
    }
    return obj;
};

$(document).ready(function() {
    $('#dataTables').DataTable({
        responsive: true
    });
    var dataTablesLogs = $('#dataTables-logs').DataTable();
    $('#dataTables-logs tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            dataTablesLogs.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
    function removeIdLogs(id){
        $.ajax({
            type: "POST",
            url: '/pages/logs_error.html',
            data: "r=1&delete=1&id="+id,
            success: function(data){
                location.href = '/pages/logs_error.html';
            }
        });
    }
    $('#dataTables').on('click', '.table-delete', function () {
        var t = dataTablesLe($(this).parent().parent());
        var id = t.id;
        var tr = t.tr;

        removeIdLogs(id);
    });
    $('#dataTables').on('click', '.table-search', function () {
        var coun = $(this).attr('country');
        var code = $(this).attr('code');
        var tk   = $(this).attr('tk');
        var record   = $(this).attr('record');

        var href = '/pages/logs_search_tk.html?country='+coun+'&'+'code='+code+'&'+'tk='+tk+'&rec='+record;

        location.href = href;
    });
    $('#addCity').click(function() {
        var t = $('#dataTables-logs').find('.selected');
        var tr = $("#logs-kladr").find('.table-id').parent();

        if(!t.length) return false;
        if(!tr.length) return false;

        var id    = t.find('.table-id').text();
        var kladr = tr.attr('kladr');
        var tk    = tr.attr('tk');
        var rec   = tr.attr('rec');
        var country = tr.attr('country');

        $.ajax({
            type: "POST",
            url: '/pages/logs_search_tk.html',
            data: "r=1&add=1&id="+id+"&kladr="+kladr+"&tk="+tk+"&rec="+rec+"&country="+country,
            success: function(data){
                console.log(data);
                if(data == 1){
                    $('#alert-success-removeCity').show();
                }else{
                    $('#alert-danger-removeCity').show();
                }
            }
        });
    });
    $('.removeCity').click(function() {
        var tr = $("#logs-kladr").find('.table-id').parent();
        if(!tr.length) return false;
        var rec    = tr.attr('rec');

        removeIdLogs(rec);
    });
    $('.sync-button').click(function(){
        var tk = $('.sync-value').val();
        var country = $('.sync-country').val();
        $.ajax({
            type: "POST",
            url: '',
            data: "r=1&update=1&tk="+tk+"&country="+country,
            success: function(data){
                if(data == 1){
                    $('.alert-success').show();
                }
            }
        });
    });
    $('#dataTables').on('click', '.le-table-remove', function () {
        var tr = dataTablesLe($(this).parent().parent());
        var id = tr.id;

        $.ajax({
            type: "POST",
            url: '',
            data: "r=1&update=1&id="+id,
            success: function(data){
                console.log(data);
                if(data == 1){
                    location.reload();
                }else{
                    $('#alert-danger-removeCity').show();
                }
            }
        });
    });
});
